# Apache2 Domain Tools

These tools can be used to simply create and remove virtual hosts configuration files.

**a2ad**: Adding a virtual host currently contains:

* Domain name (with subdomain)
* Check if domain name already exists (not yet for aliasses)
* When no subdomain is provided you can add the optional "www" alias
* Add custom server aliasses
* Logging to default apache2 log or custom domain log (path will follow)
* Subdomain with underscore (_) detection. IE cannot set cookies to subdomains containing a underscore
